//function dengan nama teriak() 
console.log("Soal no.1")
function teriak(){
   return "Halo Sanbers!";
}
console.log(teriak()) // "Halo Sanbers!" 



// function dengan nama kalikan() 
console.log("\nSoal no.2")
function kalikan(a,b){ 
    return a * b;
}
var num1 = 12
var num2 = 4
 
var hasilKali = kalikan(num1, num2)
console.log(hasilKali) // 48



// function dengan nama introduce()
console.log("\nSoal no.3")
function introduce(a,b,c,d){
    var v = "Nama saya " + a + ", umur saya " + b + " tahun," + " alamat saya di " + c + "," + " dan saya punya hobby yaitu " + d +"!";
return v;
}
var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"
 
var perkenalan = introduce(name, age, address, hobby)

console.log(perkenalan) 
// Menampilkan "Nama saya Agus, umur saya 30 tahun, alamat saya di Jln. Malioboro, Yogyakarta, dan saya punya hobby yaitu Gaming!" 