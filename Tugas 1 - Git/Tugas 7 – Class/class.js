class Animal {
    constructor(name){
        this.nama = name;
        this.legs = 4;
        this.cold_blooded = false;
    }
    get name(){
        return this.nama;
    }
    set name(x){
        this.nama = x;
    }
}

class Ape extends Animal{
    constructor(name){
        super(name)
        this.legs = 2;
    }
   
    yell(){
        return console.log('\n"Auooo"')
    }

}

class Frog extends Animal{
    constructor(name){
        super(name)
        this.legs = 2;
        this.cold_blooded = true;
    }
   
    
    jump(){
        return console.log('\n"hop hop"')
    }
}

console.log("1. Animal Class")
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"

var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 



console.log("=====================\n2. Function to Class")

class Clock {
   constructor({template}){
    this.template = template;
   
     

   }

    render(){
    var date = new Date();

    var hours = date.getHours();
    
    var mins = date.getMinutes();
    

    var secs = date.getSeconds();
    if (hours < 10) hours = '0' + hours;
    if (secs < 10) secs = '0' + secs;
    if (mins < 10) mins = '0' + mins;
   
    var output = this.template
     .replace('h', hours)
     .replace('m', mins)
     .replace('s', secs);

  console.log(output);
    }


    start = function() {
        
        this.render()
        this.timer = setInterval(this.render.bind(this), 1000);
      };

    stop = function() {
        clearInterval(this.timer);
      };
}

var clock = new Clock({template: 'h:m:s'});
clock.start();  