console.log("A. Balik String")
//A. Balik String

function balikString(a) {
    var stringlama = a;
    var newstring = '';
    for(var i=a.length - 1; i>=0;i--){
        newstring = newstring + stringlama[i];
    }

    return newstring;
  }
  

console.log(balikString("abcde")) // edcba
console.log(balikString("rusak")) // kasur
console.log(balikString("racecar")) // racecar
console.log(balikString("haji")) // ijah


console.log("\nB.PALINDROME")
//B. Palindrome 

function palindrome(a) {
    var stringlama = a;
    var newstring = '';
    for(var i=a.length - 1; i>=0;i--){
        newstring = newstring + stringlama[i];
    }

    if(newstring == stringlama){
        return true;
    }
    else {
        return false;
    }
    
    
  }

  // TEST CASES Palindrome
console.log(palindrome("kasur rusak")) // true
console.log(palindrome("haji ijah")) // true
console.log(palindrome("nabasan")) // false
console.log(palindrome("nababan")) // true
console.log(palindrome("jakarta")) // false



console.log("\nC. Bandingkan Angka")
//C. Bandingkan Angka

function bandingkan(num1, num2) {
   if(num1 >= 0 && num2 >= 0){
    
     if(num2 > num1){
       
       return num2;
   }
    else if(num1==num2){
        return -1;
    }
 }

else if (num1==null || num2==null){
    if(num1==null){
        return num2;
    }
    else if(num2==null){
        return num1;
    }
    
    else{return -1;}
}
else if (num1==undefined && num2==undefined){
    return -1;}

   else{
    return -1;
   }
  }

  // TEST CASES Bandingkan Angka
console.log(bandingkan(10, 15)); // 15
console.log(bandingkan(12, 12)); // -1
console.log(bandingkan(-1, 10)); // -1 
console.log(bandingkan(112, 121));// 121
console.log(bandingkan(1)); // 1
console.log(bandingkan()); // -1
console.log(bandingkan("15", "18")) // 18